import express from 'express';
import bodyParser from 'body-parser';
import rp from 'request-promise';
import config from './config';
import pluralize from 'pluralize';
import path from 'path';

let map = [], mappedWords = [];
let port = config.port;
let app = express();
let URL = 'http://terriblytinytales.com/test.txt';

//Attach middlewares to app instance:
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, '../public')));
app.set('view engine', 'hbs');

//GET '/' home route:
app.get('/', (req, res) => {
    //return the UI for form:
    res.render('index');
});

//POST '/' home route:
app.post('/', (req, res) => {
    let n = req.body.number;
    let tempArray = [], sentenceArray = [], wordsArray = [];
    if (n > 0 && parseInt(n) == n) {
        //Initiate request to fetch content:
        rp.get(URL).then((content) => {
            let array = content.split('\n\n');
            //Remove all the special charecters and numbers from the text:
            array.forEach(element => {
                element = element.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '').replace(/[0-9]/g, '');
                tempArray.push(element.toLowerCase());
            });
            //Create a sentence array:
            tempArray.forEach(element => {
                element.split('\n').forEach(e => {
                    sentenceArray.push(e);
                });
            });
            //Chunk sentences into words:
            sentenceArray.forEach(sentence => {
                sentence.split(' ').forEach(word => {
                    word.split('\t').forEach(w => {
                        wordsArray.push(w);
                    });
                });
            });
            //Singularize all the words in the words array:
            for (let i = 0; i < wordsArray.length; i++) {
                wordsArray[i] = pluralize.singular(wordsArray[i]);
            }
            //Calculate frequency of each word in the text:
            wordsArray.forEach(word => {
                if (word != '' && mappedWords.indexOf(word) == -1 && word != '-') {
                    map.push([word, getFrequency(word, wordsArray)]);
                    mappedWords.push(word);
                }
            });
            //Sort the map in descending order:
            map = map.sort((a, b) => { return a[1] > b[1] ? -1 : a[1] < b[1] ? 1 : 0 });
            map.length > n ? res.json(map.slice(0, n)) : res.json(map);
        }).catch((err) => {
            res.json(err);
        });
    } else {
        res.json([]);
    }
});

app.listen(port, () => {
    console.log('App listening on port ' + config.port);
});

function getFrequency(w, array) {
    var count = 0;
    array.forEach(element => {
        if (element == w) {
            ++count;
        }
    });
    return count;
}

module.exports = app;
