const productionConfig = {
    port: process.env.PORT,
    env: 'prod'
}

const devConfig = {
    port: 4000,
    env: 'dev'
}

module.exports = (process.env.NODE_ENV == 'prod' ? productionConfig : devConfig); 
