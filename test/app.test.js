let app = require('../build/app');
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

describe('Test edge cases for /POST route', () => {
    it('Test route for very large positive integer', (done) => {
        chai.request(app).post('/').send({ 'number': 10000 }).end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('array');
            console.log(res.body.length);
            if(res.body.length == 308){
                done();
            }else{
                throw new Error('Array length mismatch');
            }
        });
    });

    it('Test route for positive integer', (done) => {
        chai.request(app).post('/').send({ 'number': 10 }).end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('array');
            if(res.body.length == 10){
                done();
            }else{
                throw new Error('Array length mismatch');
            }
        });
    });

    it('Test route for a floating point number', (done) => {
        chai.request(app).post('/').send({ 'number': 4.6 }).end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('array');
            if(res.body.length == 0){
                done();
            }else{
                throw new Error('Array length mismatch');
            }
        });
    });


    it('Test route for negative integer', (done) => {
        chai.request(app).post('/').send({ 'number': -1 }).end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('array');
            if(res.body.length == 0){
                done();
            }else{
                throw new Error('Array length mismatch');
            }
        });
    });
    it('Test route for no input or zero', (done) => {
        chai.request(app).post('/').send({ 'number': 0 }).end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('array');
            if(res.body.length == 0){
                done();
            }else{
                throw new Error('Array length mismatch');
            }
        });
    });
});

describe('/GET homepage', () => {
    it('Should fetch home page', (done) => {
        chai.request(app).get('/').end((err, res)=>{
            res.should.have.status(200);
            done();
        })
    });
});
