# TTT-Excercise
A simple excercise built and tested with love for TTT.

Find code walkthrough in the file src/app.js

###Modules Used:

###### babel: For ES6 to ES5 transpiling
###### pluralize: To convert plural words to singular
###### express: For server setup
###### hbs: View engine
###### request & request-promsie: For http requests

### Version
0.1.0

### Installation

Run the following commands to start the application

```sh
$ npm install
```

```sh
$ npm start
```
###Test

![alt text](https://s3.amazonaws.com/bucket.invenlook.storage/test_results.png)

###Deployment

Find heroku deployment link here: https://afternoon-crag-27753.herokuapp.com/



