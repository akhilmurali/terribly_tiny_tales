$(document).ready(function () {
    $('#submit-form ').click(function (e) {
        e.preventDefault();
        //console.log('Button clicked');
        console.log($('#number').val());
        $.post("/", { number: $('#number').val() }, function (data) {
            try{
                if(data.length > 0){
                    $('#table').css('display', 'block');
                    $('#Error').css('display', 'none');
                    console.log(data.length);
                    $('tbody').empty();
                    for(let i = 0; i < data.length; i++){
                        console.log(data[i][0], data[i][1]);
                        $('tbody').append(`<tr class='table-light'><th scope="col">${data[i][0]}</th>
                        <th scope="col">${data[i][1]}</th></tr>`)
                    }
                }else{
                    $('#table').css('display', 'none');
                    $('#Error').css('display', 'block');
                }
            }catch(e){
                console.log('Error');
            }
        }, "json");
    });
}); 